package cz.fel.cvut.ts1;

public class Jelinad5 {
    public long factorial (int n){
        if (n == 0){
            return 1;
        } else {
            return n * factorial(n-1);
        }
    }
}
