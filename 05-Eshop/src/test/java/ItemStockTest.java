import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import shop.Item;
import org.junit.jupiter.api.Test;
import shop.StandardItem;
import storage.ItemStock;
import storage.NoItemInStorage;


public class ItemStockTest {
    Item item;

    ItemStock itemStock;

    @BeforeEach
    public void setup() {
        item = new StandardItem(1, "Item01", 10, "Category01", 1);
        itemStock = new ItemStock(item);
        itemStock.IncreaseItemCount(1);
    }

    @Test
    public void ItemStock_Constructor_Succeeds(){
        int answer_count = itemStock.getCount();
        int answer_id = itemStock.getItem().getID();
        String answer_name = itemStock.getItem().getName();
        float answer_price = itemStock.getItem().getPrice();
        String answer_category = itemStock.getItem().getCategory();

        Assertions.assertEquals(1, answer_count);
        Assertions.assertEquals(1, answer_id);
        Assertions.assertEquals("Item01", answer_name);
        Assertions.assertEquals(10, answer_price);
        Assertions.assertEquals("Category01", answer_category);
    }

    @ParameterizedTest(name = "1 plus {0} should be equal to {1}, {1} minus {2} should be equal to {3}")
    @CsvSource({"2, 3, 1, 2", "4, 5, 3, 2", "7, 8, 2, 6"})
    public void IncreaseItemCount_DecreaseItemCount_IncreaseAndDecreasesCount_Succeeds(int a, int b, int c, int d) throws NoItemInStorage {
        itemStock.IncreaseItemCount(a);
        int count_answer = itemStock.getCount();

        Assertions.assertEquals(count_answer, b);

        itemStock.decreaseItemCount(c);
        count_answer = itemStock.getCount();
        Assertions.assertEquals(count_answer, d);

    }
}
