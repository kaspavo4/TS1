import cz.fel.cvut.calculator.Calculator;
import org.junit.jupiter.api.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class CalculatorTest {
    static Calculator calculator;

    @BeforeAll
    public static void initVariable() {
        calculator = new Calculator();
    }
    @Test
    @Order(1)
    public void add_addition_return8(){
        //ARRANGE
        int Int_a = 5;
        int Int_b = 3;

        //ACT
        int answer = calculator.add(Int_a, Int_b);

        //ASSERT
        Assertions.assertEquals(8, answer);
    }
    @Test
    @Order(2)
    public void subtract_subtraction_return5(){
        //ARRANGE
        int Int_a = 8;
        int Int_b = 3;

        //ACT
        int answer = calculator.subtract(Int_a, Int_b);

        //ASSERT
        Assertions.assertEquals(5, answer);
    }
    @Test
    @Order(3)
    public void multiply_multiplication_return10(){
        //ARRANGE
        int Int_a = 5;
        int Int_b = 2;

        //ACT
        int answer = calculator.multiply(Int_a, Int_b);

        //ASSERT
        Assertions.assertEquals(10, answer);
    }
    @Test
    @Order(4)
    public void divide_division_return3() throws Exception {
        //ARRANGE
        int Int_a = 15;
        int Int_b = 5;

        //ACT
        int answer = calculator.divide(Int_a, Int_b);

        //ASSERT
        Assertions.assertEquals(3, answer);
    }

    @Test
    public void divide_exceptionThrown_exception() {
        //ARRANGE
        int Int_a = 15;
        int Int_b = 0;


        //ACT + (ASSERT)
        Exception exception = Assertions.assertThrows(Exception.class, () -> calculator.divide(Int_a,Int_b));

        String expectedMessage = "Cant divide number by 0";
        String actualMessage = exception.getMessage();

        //ASSERT 2
        Assertions.assertEquals(expectedMessage, actualMessage);
    }
}
