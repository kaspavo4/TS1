import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;

public class Springer_articlePage_model {

    private WebDriver driver;
    private WebDriverWait wait;

    @FindBy(className = "c-article-title")
    private WebElement Title;

    @FindBy(id = "article-info-content")
    private WebElement info_content;

    @FindBy(className = "c-article-identifiers__item")
    private List <WebElement> items;

    public Springer_articlePage_model(WebDriver driver){
        this.driver = driver;
        wait = new WebDriverWait(driver, Duration.ofSeconds(3));
        PageFactory.initElements(driver, this);
    }

    public String Return_title(){
        wait.until(ExpectedConditions.visibilityOf(Title));
        return Title.getText();
    }

    public String Return_date(){
        wait.until(ExpectedConditions.visibilityOf(Title));
        for (WebElement value : items){
            if(value.getText().contains("Published:")){
                return value.findElement(By.tagName("time")).getText();
            }
        }
        return null;
    }

    public String Return_DOI(){
        wait.until(ExpectedConditions.visibilityOf(info_content));
        List<WebElement> values = info_content.findElements(By.className("c-bibliographic-information__value"));
        for (WebElement value : values){
            if(value.getText().contains("doi.org")){
                return value.getText();
            }
        }
        return null;
    }
}
