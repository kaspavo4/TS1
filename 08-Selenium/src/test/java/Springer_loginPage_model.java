import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class Springer_loginPage_model {
    private WebDriver driver;
    private WebDriverWait wait;

    @FindBy(id = "login-box-email")
    private WebElement email_box;

    @FindBy(id = "login-box-pw")
    private WebElement password_box;

    @FindBy(className = "btn-monster")
    private WebElement submit_button;

    public Springer_loginPage_model(WebDriver driver){
        this.driver = driver;
        wait = new WebDriverWait(driver, Duration.ofSeconds(3));
        PageFactory.initElements(driver, this);

    }

    private void set_email(String email){
        email_box.sendKeys(email);
    }
    private void set_password(String password){
        password_box.sendKeys(password);
    }

    public void login(String email, String password){
        wait.until(ExpectedConditions.visibilityOf(submit_button));
        set_email(email);
        set_password(password);
        submit_button.click();
    }
}
