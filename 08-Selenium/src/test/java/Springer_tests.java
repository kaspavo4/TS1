import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import javax.naming.Name;
import java.time.Duration;
import java.util.stream.Stream;

public class Springer_tests {

    private WebDriver driver;
    private Springer_homePage_model homepage;
    private Springer_loginPage_model login;
    private Springer_advancedSearchpage_model search;
    private Springer_resultsPage_model results;
    private Springer_articlePage_model article;


    static String[] Names;
    static String[] DOI;
    static String[] Dates;


    @BeforeAll
    public static void Setup_saved_data(){
        Names = new String[4];
        DOI = new String[4];
        Dates = new String[4];
    }

    @BeforeEach
    public void Setup(){
        driver = new FirefoxDriver();
        homepage = new Springer_homePage_model(driver);
        login = new Springer_loginPage_model(driver);
        search = new Springer_advancedSearchpage_model(driver);
        results = new Springer_resultsPage_model(driver);
        article = new Springer_articlePage_model(driver);
    }

    @Test
    public void SaveData_from_website(){
        homepage.go_to_advancedSearch();
        search.advanced_search("Page Object Model", "", "Selenium Testing", "", "", "", "in", "2023", "2023", true);
        results.set_content_type("Article");
        for (int i = 0; i <4 ; i++) {
            results.access_nPlaced_result(i);
            Names[i] = article.Return_title();
            DOI[i] = article.Return_DOI();
            Dates[i] = article.Return_date();
            driver.navigate().back();
        }

        for (int i = 0; i < 4; i++) {
            Assertions.assertNotEquals(null, Names[i]);
            Assertions.assertNotEquals(null, DOI[i]);
            Assertions.assertNotEquals(null, Dates[i]);
        }
        driver.close();
    }

    @ParameterizedTest(name = "Info from article {0} should be equal to {1} and {2}")
    @MethodSource({"article_info"})
    public void SavedData_equals_searchByName(String title, String date, String doi){
        homepage.go_to_loginPage();
        login.login("VojtaHomeRun1@gmail.com", "Neprolomitelne_123");
        homepage.go_to_advancedSearch();
        search.advanced_search("", "", "", "", title, "", "in","", "", true);
        results.access_nPlaced_result(0);
        Assertions.assertEquals(date, article.Return_date());
        Assertions.assertEquals(doi, article.Return_DOI());
        driver.close();

    }

    static Stream<Arguments> article_info() {
        return Stream.of(
                Arguments.of(Names[0], Dates[0], DOI[0]),
                Arguments.of(Names[1], Dates[1], DOI[1]),
                Arguments.of(Names[2], Dates[2], DOI[2]),
                Arguments.of(Names[3], Dates[3], DOI[3])
        );
    }

}
