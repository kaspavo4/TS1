import archive.ItemPurchaseArchiveEntry;
import archive.PurchasesArchive;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockedConstruction;
import shop.Item;
import shop.Order;
import shop.ShoppingCart;
import shop.StandardItem;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;

import static org.mockito.Mockito.*;

public class PurchasesArchiveTest {

    PurchasesArchive purchasesArchive;
    ArrayList<Item> items;
    ShoppingCart cart;
    Order order;
    Item item;
    private ByteArrayOutputStream outContent;


    @BeforeEach
    public void setup(){
        item = new StandardItem(1, "Item01", 10, "Category01", 10);
        purchasesArchive = new PurchasesArchive();
        items = new ArrayList<>();
        items.add(item);
        cart = new ShoppingCart(items);
        order = new Order(cart, "name", "address");
        outContent = new ByteArrayOutputStream();
    }

    @Test
    public void putOrderToPurchasesArchive_ItemPurchaseArchiveEntryConstructorGetsCalled_Succeeds(){
        ItemPurchaseArchiveEntry mockedItemPurchaseArchiveEntry = mock(ItemPurchaseArchiveEntry.class);
        when(mockedItemPurchaseArchiveEntry.getRefItem()).thenReturn(item);
        when(mockedItemPurchaseArchiveEntry.getCountHowManyTimesHasBeenSold()).thenReturn(1);
        purchasesArchive.putOrderToPurchasesArchive(order);

        Assertions.assertEquals(purchasesArchive.getHowManyTimesHasBeenItemSold(mockedItemPurchaseArchiveEntry.getRefItem()), mockedItemPurchaseArchiveEntry.getCountHowManyTimesHasBeenSold());
        verify(mockedItemPurchaseArchiveEntry,times(1)).getRefItem();
        verify(mockedItemPurchaseArchiveEntry,times(1)).getCountHowManyTimesHasBeenSold();
    }

    @Test
    public void printItemPurchaseStatistics_printsItemStatistics_CorrectTextCaptured(){
        purchasesArchive.putOrderToPurchasesArchive(order);

        System.setOut(new PrintStream(outContent));
        purchasesArchive.printItemPurchaseStatistics();
        Assertions.assertEquals ("ITEM PURCHASE STATISTICS:\r\n" +
                "ITEM  Item   ID 1   NAME Item01   CATEGORY Category01   PRICE 10.0   LOYALTY POINTS 10   HAS BEEN SOLD 1 TIMES\r\n", outContent.toString());


    }

    @Test
    public void getHowManyTimesHasBeenItemSold_returnsNumberOfSold_returnsCorrect(){
        PurchasesArchive mockedPurchaseArchive = mock(PurchasesArchive.class);
        purchasesArchive.putOrderToPurchasesArchive(order);
        when(mockedPurchaseArchive.getHowManyTimesHasBeenItemSold(item)).thenReturn(1);

        Assertions.assertEquals(purchasesArchive.getHowManyTimesHasBeenItemSold(item), mockedPurchaseArchive.getHowManyTimesHasBeenItemSold(item));
        verify(mockedPurchaseArchive,times(1)).getHowManyTimesHasBeenItemSold(any(Item.class));
    }
}
