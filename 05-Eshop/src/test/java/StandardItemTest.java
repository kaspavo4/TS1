import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;
import shop.StandardItem;
import org.junit.jupiter.api.*;

import java.util.stream.Stream;

public class StandardItemTest {

    StandardItem standardItem;

    @BeforeEach
    public void setup() {
        standardItem = new StandardItem(1,"Item01", 10, "Category01", 2);
    }

    @Test
    public void StandardItem_constructs_succeeds (){

        int id_answer = standardItem.getID();
        String name_answer = standardItem.getName();
        float price_answer = standardItem.getPrice();
        String category_answer = standardItem.getCategory();
        int loyaltyPoints_answer = standardItem.getLoyaltyPoints();

        Assertions.assertEquals(1, id_answer);
        Assertions.assertEquals("Item01", name_answer);
        Assertions.assertEquals(10, price_answer);
        Assertions.assertEquals("Category01", category_answer);
        Assertions.assertEquals(2, loyaltyPoints_answer);

    }

    @Test
    public void copy_copiesItem_returnsNewItem (){

        StandardItem standardItem_answer = standardItem.copy();
        int id_answer = standardItem_answer.getID();
        String name_answer = standardItem_answer.getName();
        float price_answer = standardItem_answer.getPrice();
        String category_answer = standardItem_answer.getCategory();
        int loyaltyPoints_answer = standardItem_answer.getLoyaltyPoints();

        Assertions.assertEquals(1, id_answer);
        Assertions.assertEquals("Item01", name_answer);
        Assertions.assertEquals(10, price_answer);
        Assertions.assertEquals("Category01", category_answer);
        Assertions.assertEquals(2, loyaltyPoints_answer);
    }
    @ParameterizedTest(name = "{0} should be equal to {1}")
    @MethodSource({"standardItems"})
    public void equals_comparesObjectIfEqual_returnsTrue(StandardItem a, StandardItem b){

        boolean answer = a.equals(b);

        Assertions.assertEquals(true, answer);
    }

    static Stream<Arguments> standardItems() {
        return Stream.of(
                Arguments.of(new StandardItem(1,"Item01", 10, "Category01", 2),
                        new StandardItem(1,"Item01", 10, "Category01", 2)),
                Arguments.of(new StandardItem(2,"Item02", 30, "Category02", 10),
                        new StandardItem(2,"Item02", 30, "Category02", 10)),
                Arguments.of(new StandardItem(3,"Item03", 0, "Category03", 20),
                        new StandardItem(3,"Item03" , 0, "Category03", 20))
        );
    }
}
