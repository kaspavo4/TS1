import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import shop.Item;
import shop.Order;
import shop.ShoppingCart;
import shop.StandardItem;

import java.util.ArrayList;

public class OrderTest {

    @Test
    public void Order_constructsWithoutStateWithItems_succeeds(){
        ArrayList<Item> items = new ArrayList<>();
        items.add(new StandardItem(1, "Item01", 10, "Category01", 10));
        ShoppingCart cart = new ShoppingCart(items);

        Order order = new Order(cart, "name", "address");
        ArrayList<Item> items_answer = order.getItems();
        String name_answer = order.getCustomerName();
        String address_answer = order.getCustomerAddress();
        int state_answer = order.getState();

        Assertions.assertEquals(items, items_answer);
        Assertions.assertEquals("name", name_answer);
        Assertions.assertEquals("address", address_answer);
        Assertions.assertEquals(0, state_answer);
    }
    @Test
    public void Order_constructsWithStateWithoutItems_succeeds(){
        ArrayList<Item> items = new ArrayList<>();
        ShoppingCart cart = new ShoppingCart();

        Order order = new Order(cart, "name", "address", 3);
        ArrayList<Item> items_answer = order.getItems();
        String name_answer = order.getCustomerName();
        String address_answer = order.getCustomerAddress();
        int state_answer = order.getState();

        Assertions.assertEquals(items, items_answer);
        Assertions.assertEquals("name", name_answer);
        Assertions.assertEquals("address", address_answer);
        Assertions.assertEquals(3, state_answer);
    }

    @Test
    public void Order_constructsWithNulls_succeeds(){
        ArrayList<Item> items = new ArrayList<>();
        ShoppingCart cart = new ShoppingCart();

        Order order = new Order(cart, null, null);
        ArrayList<Item> items_answer = order.getItems();
        String name_answer = order.getCustomerName();
        String address_answer = order.getCustomerAddress();
        int state_answer = order.getState();

        Assertions.assertEquals(items, items_answer);
        Assertions.assertEquals(null, name_answer);
        Assertions.assertEquals(null, address_answer);
        Assertions.assertEquals(0, state_answer);
    }
}
